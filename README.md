# Another Castle

A simple Godot-engine game meant to demonstrate the basics of creating a 2D
platformer.

Uses assets from the following:

3 Character Sprite Sheets by CraftPix.net 2D Game Assets    
https://opengameart.org/content/3-character-sprite-sheets    
(OGA-BY 3.0 - http://static.opengameart.org/OGA-BY-3.0.txt)

Abstract Platformer by Kenney    
https://kenney.nl/assets/abstract-platformer    
(CC0 1.0 Universal - https://creativecommons.org/publicdomain/zero/1.0/)