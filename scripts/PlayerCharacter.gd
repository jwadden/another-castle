extends KinematicBody2D

export var run_speed = 400.0
export var jump_speed = 600.0
export var fall_gravity = 1200.0
export var max_fall_speed = 600.0

var velocity = Vector2(0, 0)

func _ready():
	pass

func _physics_process(delta):
	handle_gravity(delta)
	handle_left_right_input()
	handle_jump()
	
	velocity = move_and_slide(velocity, Vector2(0, -1))

func handle_gravity(delta):
	if velocity.y < 0 and Input.is_action_pressed('player_jump'):
		velocity.y += fall_gravity / 2 * delta
	else:
		velocity.y += fall_gravity * delta
	
	if velocity.y > max_fall_speed:
		velocity.y = max_fall_speed

func handle_left_right_input():
	if Input.is_action_pressed('player_left'):
		velocity.x = -run_speed
		$AnimatedSprite.flip_h = true
		if is_on_floor():
			$AnimatedSprite.play('run')
	elif Input.is_action_pressed('player_right'):
		velocity.x = run_speed
		$AnimatedSprite.flip_h = false
		if is_on_floor():
			$AnimatedSprite.play('run')
	else:
		velocity.x = 0
		if is_on_floor():
			$AnimatedSprite.play('idle')

func handle_jump():
	if is_on_floor() and Input.is_action_just_pressed('player_jump'):
		velocity.y = -jump_speed
		$AnimatedSprite.play('jump')
